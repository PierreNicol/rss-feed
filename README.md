# RSS Feed
Flux RSS is a simple Android application written in Java that allows you to see the 
latest news on Le Monde RSS Feed "A la une". 
It is based on a practical work I had to do at my school: [ENSICAEN](https://www.ensicaen.fr/)

## Getting Started

This application uses the Gradle build system.

1. Download the samples by cloning this repository or downloading an archived snapshot. (See the options at the top of the page.)
2. In Android Studio, create a new project and choose the "Import non-Android Studio project" or "Import Project" option.
3. Select the directory that you downloaded with this repository.
4. If prompted for a gradle configuration, accept the default settings. Alternatively use the "gradlew build" command to build the project directly.

### Prerequisites

* Android SDK v28
* Latest Android Build Tools
* Android Support Repository

## Built With

* [Gradle](https://gradle.org/) - Dependency Management
* [Lombok](https://projectlombok.org/)

## Author

**Pierre Nicol** https://www.pierrenicol.net

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details
